An identifier for an aggregate instance

# Responsibilities
Identifies an aggregate instance uniquely in a given context.
Provides the following components:
- optionally organization/domain
- Bounded Context (e.g. Aktenordner)
- Aggregate Type (e.g. Vorgang)
- Instance identifier
	- must be unique
# Colaborations
{describe colaborations}
