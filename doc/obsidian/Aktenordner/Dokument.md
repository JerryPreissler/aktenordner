Repräsentiert ein elektronisches oder physikalisches Schriftstück oder anderes Dokument.

# Responsibilities
Muss eine Kopie oder Repräsentation des Schriftstücks im System vorhalten und zugänglich machen.
Muss entsprechende Metadaten verwalten und suchbar machen.

# Colaborations
Kann im [[Eingang]] vorgehalten werden.
Kann einen [[Vorgang]] initiieren.
Kann einem [[Vorgang]] zugeordnet werden.
Wird von/an einen [[Korrespondent]] empfangen/gesendet/weitergeleitet.
[[Erzeuger]]
Eigentümer

#Properties
- Datum wann angelegt
- Inhalt*
	- Blobs mit Scans, EMails, whatever