Entität die eingehende [[Dokument]]e vorhält ehe sie einem [[Korrespondent]] zugeordnet werden.

# Responsibilities
{describe responsibilities}
# Colaborations
| Name                   | Rolle |
|------------------------| - |
| [[Dokument]]           |  |
| [[Korrespondent]] (pl) | Die Kommunikationspartner denen ein eingehendes Dokument zugeordnet werden kann |
