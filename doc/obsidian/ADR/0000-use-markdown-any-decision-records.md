---
status: "accepted"
date: 2023-04-29
---
# Use [markdown any decision records](https://adr.github.io/madr/) to document ADRs

## Context and Problem Statement

I need a format to document ADRs. There are lots of options to choose from (see [here]() for some options) but I don't want to start a new project for selection. 

## Decision Drivers

* Need to pick one, take this and strip down to the amount I actually need
* Need to find out what I actually need anyhow

## Considered Options

* [Markdown Any Decision Records](https://adr.github.io/madr/)
* Freeform

## Decision Outcome
Chosen MADR because it looks like it can work, I need to try out a format anyhow and the decision should be easily reconsidered while not too many ADRs have been created. 
