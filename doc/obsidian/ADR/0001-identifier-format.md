---
status: "accepted"
date: 2023-04-30
---
# Use Java-UUID as core format for aggregate identifiers

## Context and Problem Statement

[[AggregateId]]s need a representation of an identifier that can be easily be used with persistence solutions and can be represented in a serializable format. 

## Decision Drivers

* Can the solution be used with any foreseeable persistence solution?
* Can the solution be used in a REST and Web API?
* Does the solution have development impacts?
* Does the solution have performance impacts?
* Futureproof?
* Does the solution work with multiple instances of the program?

## Considered Options

* Incrementing integer
* Java default [UUID](https://docs.oracle.com/javase/8/docs/api/java/util/UUID.html)
* [Time-Sorted Unique Identifiers](https://vladmihalcea.com/uuid-database-primary-key/)
* Persistence-specific ID

## Decision Outcome

Going with Java default UUID.

### Consequences

* Good: 
	* no external dependencies required
	* well documented and supported 
	* easy to implement
	* [well supported in MongoDB](https://stackoverflow.com/questions/28895067/using-uuids-instead-of-objectids-in-mongodb)
* Bad
	* performance impact for persistence (not expected to become relevant in the scope of this program)
	* larger size than other solutions especially when serialized to ASCII
	* quasi-opaque for debugging purposes
	
## Validation

{describe how the implementation of/compliance with the ADR is validated. E.g., by a review or an ArchUnit test}

<!-- This is an optional element. Feel free to remove. -->
## Pros and Cons of the Options

### {title of option 1}

<!-- This is an optional element. Feel free to remove. -->
{example | description | pointer to more information | …}

* Good, because {argument a}
* Good, because {argument b}
<!-- use "neutral" if the given argument weights neither for good nor bad -->
* Neutral, because {argument c}
* Bad, because {argument d}
* … <!-- numbers of pros and cons can vary -->

### {title of other option}

{example | description | pointer to more information | …}

* Good, because {argument a}
* Good, because {argument b}
* Neutral, because {argument c}
* Bad, because {argument d}
* …

<!-- This is an optional element. Feel free to remove. -->
## More Information

{You might want to provide additional evidence/confidence for the decision outcome here and/or
 document the team agreement on the decision and/or
 define when and how this decision should be realized and if/when it should be re-visited and/or
 how the decision is validated.
 Links to other decisions and resources might appear here as well.}
