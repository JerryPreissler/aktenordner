---
status: "proposed"
date: 2023-05-18
---
# Use kotlinx.serialization for JSON (de)serialization

## Context and Problem Statement

{Describe the context and problem statement, e.g., in free form using two to three sentences or in the form of an illustrative story.
 You may want to articulate the problem in form of a question and add links to collaboration boards or issue management systems.}

## Decision Drivers

* additional dependencies
* ktor integration
* performance
* impact on program structure

## Considered Options

* kotlinx.serialization
* [moshi](https://github.com/square/moshi)
* jackson
* gson

## Decision Outcome

Going with kotlinx.serialization
Place serializers for domain members in org/codeshards/<context>/domain/Serializers.kt
	(+) no dependency from domain to other packages
	(-) technical stuff in domain implementation


<!-- This is an optional element. Feel free to remove. -->
### Consequences

* (+) ktor supports kotlinx.serialization out of the box -> Content Negotiation works out of the box
* (+) no reflection needed -> better performance expected
* (-) Need to implement serializers for non-trivial member types, e.g. UUID, LocalDateTime
* (-) Need to put implementation for serializers somewhere


