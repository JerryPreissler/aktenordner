---
status: "proposed"
date: 2023-06-09
---
# Use [Hoplite](https://github.com/sksamuel/hoplite) for configuration

## Context and Problem Statement

aktenordner needs a configuration mechanism to easily provide settings for components within the application.

Examples:
* Base URL for the application (needed in Rest/Html adapters)
* Location of File-based storage
* Database coordinates and credentials

## Decision Drivers

* Need to pick one, take this and strip down to the amount I actually need
* Need to find out what I actually need anyhow

## Considered Options

* [Apache Commons Configuration](https://commons.apache.org/proper/commons-configuration/)
* [Hoplite](https://github.com/sksamuel/hoplite)
* Own implementation

## Decision Outcome
Test drive Hoplite as the first option. 

Pros:
* Kotlin native so should be usable in multiplatform if needed. 
* Lots of definitions supported.
* Active development

Cons:
* Not as established as an Apache project -> is it future proof?

## Validation
* How does it work, how much effort is it to make config available to components?
* Can it be integrated with dependency injection?
* Can it be used to define DI modules?