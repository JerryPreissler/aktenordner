This project contains my attempt to implement a personal document management app following the principles outlined in [Tom Homberg's book "Get you hands dirty on clean architecture"](https://reflectoring.io/book/) using Kotlin-based frameworks and utilities, namely [ktor](https://ktor.io/), [koin](https://insert-koin.io/), kotlinx [serialization](https://github.com/Kotlin/kotlinx.serialization) and [html](https://github.com/Kotlin/kotlinx.html).

Warning: as of now this project is in very early stages of implementation. It is uploaded mainly to provide a reference to discuss problems I'm running in with the implementation. It is not fit for any practical purpose right now.

Copyright 2023 Gerald Preissler

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
