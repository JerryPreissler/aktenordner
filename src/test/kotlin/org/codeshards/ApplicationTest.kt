package org.codeshards

import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.server.testing.*
import io.ktor.http.*
import org.codeshards.plugins.*
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class ApplicationTest {
    @Test
    fun testRoot() = testApplication {
        application {
            configureRouting()
        }
        client.get("/").apply {
            Assertions.assertEquals(HttpStatusCode.OK, status)
            Assertions.assertEquals("Hello World!", bodyAsText())
        }
    }
}