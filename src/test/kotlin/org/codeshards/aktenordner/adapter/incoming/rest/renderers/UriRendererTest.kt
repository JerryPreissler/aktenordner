package org.codeshards.aktenordner.adapter.incoming.rest.renderers

import io.ktor.http.Url
import org.codeshards.aktenordner.domain.DokumentId
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class UriRendererTest {

    @Test
    fun testDokumentId() {
        val id = DokumentId()
        val expected = Url(
            "http://localhost:8080/aktenordner/dokument/${id.instance.toString()}"
        )
        val url = UriRenderer.render(id)
        assertEquals(expected, url)
        val baseUrl = UriRenderer.BASE_URL
        val expected_base = Url(
            "http://localhost:8080"
        )
        // make sure base builder is unchanged
        assertEquals(expected_base, baseUrl)
    }
}