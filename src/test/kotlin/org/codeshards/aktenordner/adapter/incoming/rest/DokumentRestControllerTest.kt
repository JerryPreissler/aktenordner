package org.codeshards.aktenordner.adapter.incoming.rest

import io.ktor.client.request.*
import io.ktor.client.request.forms.*
import io.ktor.client.statement.*
import io.ktor.http.*
import io.ktor.server.testing.*
import org.codeshards.aktenordner.adapter.incoming.rest.renderers.UriRenderer
import org.codeshards.aktenordner.adapter.outgoing.persistence.memoryPersistenceKoinTestModule
import org.hamcrest.CoreMatchers.containsString
import org.hamcrest.MatcherAssert.assertThat
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.RegisterExtension
import org.koin.test.KoinTest
import org.koin.test.junit5.KoinTestExtension

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class DokumentRestControllerTest : KoinTest {

//    val CONTENT_TYPE_JSON = "application/json"
    val CONTENT_TYPE_FORM_ENCODED = "application/x-www-form-urlencoded"

    @JvmField
    @RegisterExtension
    @Suppress("unused")
    val koinTestExtension = KoinTestExtension.create {
        modules(
            restKoinModule,
            memoryPersistenceKoinTestModule,
        )
    }

    val presentId = org.codeshards.aktenordner.adapter.outgoing.persistence.presentId
    val presentAsString = presentId.instance.toString()

    @Test
    fun testGetDokumentOk() = testApplication {
        application {
            dokumentModule()
        }
        // Dokument(presentId) is added in persistenceKoinTestModule
        client.get("/aktenordner/dokument/${presentAsString}").apply {
            assertEquals(HttpStatusCode.OK, status)
            assertThat(bodyAsText(),containsString("\"id\":\"$presentAsString\""))
        }
    }

    @Test
    fun testGetDokumentUnknown() = testApplication {
        application {
            dokumentModule()
        }        // call with unknown id should result in 404
        client.get("/aktenordner/dokument/e58ed763-928c-4155-bee9-fdbaaadc15ff").apply {
            assertEquals(HttpStatusCode.NotFound, status)
        }
    }

    @Test
    fun testGetDokumentFormatFail() = testApplication {
        application {
            dokumentModule()
        }
        // call with invalid id should result in 400
        client.get("/aktenordner/dokument/foo").apply {
            assertEquals(HttpStatusCode.BadRequest, status)
            assertThat(bodyAsText(), containsString("Invalid UUID format: foo", ))
        }
    }

    @Test
    fun testListDokumente() = testApplication {
        application {
            dokumentModule()
        }
        client.get("/aktenordner/dokument").apply {
            assertEquals(HttpStatusCode.OK, status)
            val text = bodyAsText()
            assertThat(text, containsString("<ul>")) // TODO provide proper regex to match expected response
            assertThat(text, containsString(UriRenderer.render(presentId).toString()))
        }
    }

    @Test
    fun testCreateDokumentForm() = testApplication {
        application {
            dokumentModule()
        }
        val response = client.submitForm(
            url = "/aktenordner/dokument",
            formParameters = parameters {
                append("titel", "test")
            },
        ) {
            io.ktor.http.headers {
                append(HttpHeaders.ContentType, CONTENT_TYPE_FORM_ENCODED)
            }
        }
        assertEquals(HttpStatusCode.Created, response.status)
        val location = response.headers.get("Location")
        assertThat("Unexpected location value $location", (location ?: "").startsWith("/aktenordner/dokument/"))
    }
}