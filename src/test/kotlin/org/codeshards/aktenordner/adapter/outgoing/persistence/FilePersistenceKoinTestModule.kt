package org.codeshards.aktenordner.adapter.outgoing.persistence

import org.codeshards.aktenordner.application.port.outgoing.ListDokumentePort
import org.codeshards.aktenordner.application.port.outgoing.ReadDokumentPort
import org.codeshards.aktenordner.application.port.outgoing.CreateDokumentPort
import org.codeshards.aktenordner.application.port.outgoing.UploadDokumentContentPort
import org.codeshards.aktenordner.domain.DokumentMeta
import org.koin.core.module.dsl.bind
import org.koin.core.module.dsl.withOptions
import org.koin.dsl.module
import kotlin.io.path.createTempDirectory

val baseDirectory: String = createTempDirectory("aktenordner.test").toString()

val filePersistenceKoinTestModule = module {
    single {
        DokumentFilePersistenceAdapter(
            baseDirectory,
            listOf<DokumentMeta>(
                entry,
                nextEntry,
            )
        )
    } withOptions {
        bind<ReadDokumentPort>()
        bind<CreateDokumentPort>()
        bind<ListDokumentePort>()
        bind<UploadDokumentContentPort>()
    }

}