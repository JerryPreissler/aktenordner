package org.codeshards.aktenordner.adapter.outgoing.persistence

import io.ktor.http.*
import org.codeshards.aktenordner.application.port.outgoing.ListDokumentePort
import org.codeshards.aktenordner.application.port.outgoing.ReadDokumentPort
import org.codeshards.aktenordner.application.port.outgoing.CreateDokumentPort
import org.codeshards.aktenordner.domain.DokumentMeta
import org.codeshards.aktenordner.domain.DokumentId
import org.codeshards.ddd.OperationResult
import org.hamcrest.MatcherAssert
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import kotlin.test.assertContains
import kotlin.test.assertEquals

abstract class CommonDokumentPersistenceAdapterTest {

    val presentId = org.codeshards.aktenordner.adapter.outgoing.persistence.presentId
    val missingId = (DokumentId.Builder().create("e58ed763-928c-4155-bee9-fdbaaadc15ff") as OperationResult.SUCCESS<DokumentId>).value

    abstract fun getListDokumentPort(): ListDokumentePort
    abstract fun getLoadDokumentPort(): ReadDokumentPort
    abstract fun getCreateDokumentPort(): CreateDokumentPort



    @Test
    fun createAndLoadOkTest() {
        val createDokumentPortTestee: CreateDokumentPort = getCreateDokumentPort()
        val readDokumentPortTestee: ReadDokumentPort = getLoadDokumentPort()
        val newId = DokumentId()
        val testMeta = DokumentMeta(newId, titel="foo")
        when (val saveResult = createDokumentPortTestee.createDokument(testMeta)) {
            is OperationResult.SUCCESS -> {
                val value = saveResult.value
                assertEquals("foo", value.meta.titel, "saveResult mismatch")
                assertEquals(newId, value.id)
            }
            else -> {
                Assertions.fail("Unexpected result during save: $saveResult")
            }
        }
        when (val loadResult = readDokumentPortTestee.readDokument(newId)) {
            is OperationResult.SUCCESS -> {
                val value = loadResult.value
                assertEquals("foo", value.meta.titel, "loadResult mismatch")
                assertEquals(newId, value.id)
            }
            else -> {
                Assertions.fail("Unexpected result during reload: $loadResult")
            }
        }
    }

    @Test
    fun loadDokumentFailTest() {
        val readDokumentPortTestee: ReadDokumentPort = getLoadDokumentPort()
        val result = readDokumentPortTestee.readDokument(missingId)
        MatcherAssert.assertThat("Unexpected result type $result::class.simpleName", result is OperationResult.ERROR)
    }

    @Test
    fun loadDokumentOkTest() {
        val readDokumentPortTestee: ReadDokumentPort = getLoadDokumentPort()
        when (val loadResult = readDokumentPortTestee.readDokument(presentId)) {
            is OperationResult.SUCCESS -> {
                val value = loadResult.value
                assertEquals("DummyDokument", value.meta.titel, "loadResult mismatch")
                assertEquals(presentId, value.id)
            }
            else -> {
                Assertions.fail("Unexpected result during load: $loadResult")
            }
        }
    }

    @Test
    fun createDuplicateDokumentTest() {
        val createDokumentPortTestee: CreateDokumentPort = getCreateDokumentPort()
        val meta = DokumentMeta(id = presentId)
        when (val result = createDokumentPortTestee.createDokument(meta)) {
            is OperationResult.ERROR -> {
                assertContains(result.message, presentId.toString())
                assertEquals(HttpStatusCode.BadRequest, result.status)
            }
            is OperationResult.SUCCESS -> {
                Assertions.fail("Should not be able to create duplicate Dokument")
            }
        }

    }

    @Test
    fun listDokumenteTest() {
        val listDokumentePortTestee: ListDokumentePort = getListDokumentPort()
        val createDokumentPortTestee: CreateDokumentPort = getCreateDokumentPort()
        val initial = listDokumentePortTestee.listDokumente()
        when (initial) {
            is OperationResult.ERROR ->
                Assertions.fail("Unexpected result during list: $initial")
            is OperationResult.SUCCESS -> {
                val initialIds = initial.value
                MatcherAssert.assertThat("Expected id missing", initialIds.contains(presentId))
                val newMeta = DokumentMeta(DokumentId())
                MatcherAssert.assertThat("New Dokument already in list", !(initialIds.contains(newMeta.id)))
                createDokumentPortTestee.createDokument(newMeta)
                val results = listDokumentePortTestee.listDokumente()
                when(results) {
                    is OperationResult.ERROR ->
                        Assertions.fail("Unexpected result during list: $initial")
                    is OperationResult.SUCCESS ->
                        MatcherAssert.assertThat(
                            "New Dokument not included in list result",
                            results.value.contains(newMeta.id)
                        )
                }
            }
        }
    }
}