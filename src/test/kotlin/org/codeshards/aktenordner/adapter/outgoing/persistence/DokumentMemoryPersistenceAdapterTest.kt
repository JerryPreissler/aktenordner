package org.codeshards.aktenordner.adapter.outgoing.persistence

import org.codeshards.aktenordner.application.port.outgoing.ListDokumentePort
import org.codeshards.aktenordner.application.port.outgoing.ReadDokumentPort
import org.codeshards.aktenordner.application.port.outgoing.CreateDokumentPort
import org.hamcrest.MatcherAssert
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.RegisterExtension
import org.koin.test.KoinTest
import org.koin.test.inject
import org.koin.test.junit5.KoinTestExtension

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class DokumentMemoryPersistenceAdapterTest: CommonDokumentPersistenceAdapterTest(), KoinTest {

    @JvmField
    @RegisterExtension
    @Suppress("unused")
    val koinTestExtension = KoinTestExtension.create {
        modules(
            memoryPersistenceKoinTestModule
        )
    }
    override fun getListDokumentPort(): ListDokumentePort {
        val listDokumentePort: ListDokumentePort by inject()
        return listDokumentePort
    }

    override fun getLoadDokumentPort(): ReadDokumentPort {
        val readDokumentPort: ReadDokumentPort by inject()
        return readDokumentPort
    }

    override fun getCreateDokumentPort(): CreateDokumentPort {
        val createDokumentPort: CreateDokumentPort by inject()
        return createDokumentPort
    }

    @Test
    fun testTesteeClassOk() {
        val listDokumentePortTestee: ListDokumentePort = getListDokumentPort()
        val createDokumentPortTestee: CreateDokumentPort = getCreateDokumentPort()
        val readDokumentPortTestee: ReadDokumentPort = getLoadDokumentPort()
        MatcherAssert.assertThat("Unexpected type $listDokumentePortTestee::class.simpleName", listDokumentePortTestee is DokumentMemoryPersistenceAdapter)
        MatcherAssert.assertThat("Unexpected type $createDokumentPortTestee::class.simpleName", createDokumentPortTestee is DokumentMemoryPersistenceAdapter)
        MatcherAssert.assertThat("Unexpected type $readDokumentPortTestee::class.simpleName", readDokumentPortTestee is DokumentMemoryPersistenceAdapter)
    }
}