package org.codeshards.aktenordner.adapter.outgoing.persistence

import io.ktor.http.*
import org.codeshards.aktenordner.application.port.incoming.UploadDokumentInhaltCommand
import org.codeshards.aktenordner.application.port.outgoing.ListDokumentePort
import org.codeshards.aktenordner.application.port.outgoing.ReadDokumentPort
import org.codeshards.aktenordner.application.port.outgoing.CreateDokumentPort
import org.codeshards.aktenordner.application.port.outgoing.UploadDokumentContentPort
import org.codeshards.aktenordner.application.service.ReadDokumentService
import org.codeshards.aktenordner.domain.Dokument
import org.codeshards.aktenordner.domain.DokumentMeta
import org.codeshards.aktenordner.domain.DokumentId
import org.codeshards.ddd.OperationResult

class DokumentMemoryPersistenceAdapter
    () : ReadDokumentPort, CreateDokumentPort, ListDokumentePort, UploadDokumentContentPort {

    private val dokumente: MutableMap<DokumentId, Dokument> = mutableMapOf()

    constructor(entries: List<DokumentMeta>): this() {
        for(entry in entries) {
            dokumente[entry.id] =
                Dokument(entry, emptyList())
        }
    }

    override fun readDokument(id: DokumentId): OperationResult<Dokument> {
        val dokument = dokumente.get(id)
        return if (dokument != null) {
            OperationResult.SUCCESS(dokument)
        } else {
            OperationResult.ERROR(
                "No Dokument found for ${id.toString()}",
                ReadDokumentService.DokumentNotFoundException(id.toString()))
        }
    }

    override fun createDokument(meta: DokumentMeta): OperationResult<Dokument> {
        if (dokumente.containsKey(meta.id)) {
            return OperationResult.ERROR(
                "Dokument ${meta.id} already exisits",
                status = HttpStatusCode.BadRequest
            )
        }
        val dokument = Dokument(meta, emptyList())
        dokumente[meta.id] = dokument
        return OperationResult.SUCCESS(dokument)
    }

    override fun listDokumente(): OperationResult<List<DokumentId>> {
        val aggregateIds = dokumente.keys.toList()
        return OperationResult.SUCCESS<List<DokumentId>>(aggregateIds)
    }

    override fun uploadContent(command: UploadDokumentInhaltCommand): OperationResult<Dokument> {
        TODO("Not yet implemented")
    }
}