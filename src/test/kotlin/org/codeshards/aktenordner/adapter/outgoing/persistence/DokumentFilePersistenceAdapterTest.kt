package org.codeshards.aktenordner.adapter.outgoing.persistence

import io.ktor.http.*
import org.codeshards.aktenordner.application.port.incoming.UploadDokumentInhaltCommand
import org.codeshards.aktenordner.application.port.outgoing.CreateDokumentPort
import org.codeshards.aktenordner.application.port.outgoing.ListDokumentePort
import org.codeshards.aktenordner.application.port.outgoing.ReadDokumentPort
import org.codeshards.aktenordner.domain.DokumentMeta
import org.codeshards.aktenordner.domain.DokumentId
import org.codeshards.ddd.OperationResult
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.RegisterExtension
import org.junit.jupiter.api.fail
import org.koin.test.KoinTest
import org.koin.test.inject
import org.koin.test.junit5.KoinTestExtension
import java.nio.file.Files
import java.nio.file.Paths
import kotlin.streams.toList

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class DokumentFilePersistenceAdapterTest: CommonDokumentPersistenceAdapterTest(), KoinTest {

    @JvmField
    @RegisterExtension
    @Suppress("unused")
    val koinTestExtension = KoinTestExtension.create {
        modules(
            filePersistenceKoinTestModule
        )
    }

    override fun getListDokumentPort(): ListDokumentePort {
        val listDokumentePort: ListDokumentePort by inject()
        return listDokumentePort
    }

    override fun getLoadDokumentPort(): ReadDokumentPort {
        val readDokumentPort: ReadDokumentPort by inject()
        return readDokumentPort
    }

    override fun getCreateDokumentPort(): CreateDokumentPort {
        val createDokumentPort: CreateDokumentPort by inject()
        return createDokumentPort
    }

    @Test
    fun testInitializer() {
        assertTrue(Files.isDirectory(testee.dataRoot))
    }

    private val testee: DokumentFilePersistenceAdapter by inject()

    @Test
    fun testCreateDokument() {
        val id = DokumentId()
        val meta = DokumentMeta(id, titel = "TestDokument")
        testee.createDokument(meta)
        val path = Paths.get(testee.dataRoot.toString(), meta.id.shortString())
        val metaPath = Paths.get(testee.dataRoot.toString(), meta.id.shortString(), "metadata.json")
        assertTrue(Files.isDirectory(path), "no path found for $path")
        assertTrue(Files.isRegularFile(metaPath), "metadata file not found at $metaPath")
    }

    @Test
    fun testLoadDokument() {
        // prepare Dokument to read
        val id = DokumentId()
        val path = Paths.get(testee.dataRoot.toString(), id.shortString())
        val metaPath = Paths.get(testee.dataRoot.toString(), id.shortString(), "metadata.json")
        assertTrue(!Files.exists(path), "file for $id already exists at $path")
        val meta = DokumentMeta(id, titel = "TestDokument")
        testee.createDokument(meta)
        assertTrue(Files.isDirectory(path), "no directory found for dokument $meta at $path")
        assertTrue(Files.isRegularFile(metaPath), "metadata file not found at $metaPath")
    }

    @Test
    fun testListDokumente() {
        val files = Files.walk(testee.dataRoot, 1).toList()
        val result: OperationResult<List<DokumentId>> = testee.listDokumente()
        when(result) {
            is OperationResult.SUCCESS -> {
                val ids = result.value
                assertEquals(files.size -1 , ids.size, "File number minus dataRoot should equal number of ids")
                val presentDokument = Paths.get(testee.dataRoot.toString(), presentId.shortString())
                assertTrue(Files.isDirectory(presentDokument), "no directory found for $presentId at $presentDokument")
            }
            is OperationResult.ERROR -> {
                fail("listDokumente resultet in OperationResult.ERROR $result")
            }
        }
    }

    @Test
    fun testUploadContent() {
        val filename = "dummy.pdf"
        val resource = this.javaClass.getResourceAsStream(filename)
        val command = UploadDokumentInhaltCommand(
            presentId,
            filename,
            resource!!
        )
        val path = Paths.get(testee.dataRoot.toString(), presentId.shortString(), filename)
        assertTrue(!Files.exists(path), "File should not exist before upload at $path")
        val result = testee.uploadContent(command)
        assertTrue(Files.exists(path), "File should exist after upload at $path")
        assertTrue(result is OperationResult.SUCCESS)
    }

    @Test
    fun testUploadContentTwice() {
        val id = DokumentId() //ensure filename is unique
        val filename = "${id.shortString()}.pdf"
        val resource = this.javaClass.getResourceAsStream("dummy.pdf")
        val command1 = UploadDokumentInhaltCommand(
            presentId,
            filename,
            resource!!
        )
        val result1 = testee.uploadContent(command1)
        assertTrue(result1 is OperationResult.SUCCESS)
        val command2 = UploadDokumentInhaltCommand(
            presentId,
            filename,
            resource
        )
        val result2 = testee.uploadContent(command2)
        when(result2) {
            is OperationResult.ERROR -> {
                assertEquals(HttpStatusCode.BadRequest, result2.status)
            }
            is OperationResult.SUCCESS -> {
                fail("uploading content with same filename as existing file should fail")
            }
        }
    }

    @Test
    fun testUploadNoDokument() {
        val id = DokumentId() // id for non-existing dokument
        val resource = this.javaClass.getResourceAsStream("dummy.pdf")
        val command = UploadDokumentInhaltCommand(
            id,
            "dummy.pdf",
            resource!!
        )
        val result = testee.uploadContent(command)
        when(result) {
            is OperationResult.ERROR -> {
                assertEquals(HttpStatusCode.NotFound, result.status)
            }
            is OperationResult.SUCCESS -> {
                fail("uploading content with same filename as existing file should fail")
            }
        }
    }
}