package org.codeshards.aktenordner.adapter.outgoing.persistence

import org.codeshards.aktenordner.application.port.outgoing.ListDokumentePort
import org.codeshards.aktenordner.application.port.outgoing.ReadDokumentPort
import org.codeshards.aktenordner.application.port.outgoing.CreateDokumentPort
import org.codeshards.aktenordner.application.port.outgoing.UploadDokumentContentPort
import org.codeshards.aktenordner.domain.DokumentMeta
import org.codeshards.aktenordner.domain.DokumentId
import org.codeshards.ddd.OperationResult
import org.koin.core.module.dsl.bind
import org.koin.core.module.dsl.withOptions
import org.koin.dsl.module

val presentId = (DokumentId.Builder().create("e58ed763-928c-4155-bee9-fdbaaadc15f3") as OperationResult.SUCCESS<DokumentId>).value
val entry = DokumentMeta(presentId, titel = "DummyDokument")
val nextEntry = DokumentMeta(DokumentId(), titel = "Another Dokument")

val memoryPersistenceKoinTestModule = module {
    single {
        DokumentMemoryPersistenceAdapter(listOf<DokumentMeta>(
            entry,
            nextEntry,
        ))
    } withOptions {
        bind<ReadDokumentPort>()
        bind<CreateDokumentPort>()
        bind<ListDokumentePort>()
        bind<UploadDokumentContentPort>()
    }

}