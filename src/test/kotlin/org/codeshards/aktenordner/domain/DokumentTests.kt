package org.codeshards.aktenordner.domain

import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import org.codeshards.aktenordner.adapter.outgoing.persistence.presentId
import org.junit.jupiter.api.Test
import java.time.LocalDateTime
import java.time.Month
import kotlin.test.assertEquals

class DokumentTests {

    val meta = DokumentMeta(
        presentId,
        LocalDateTime.of(2015,
            Month.JULY, 29, 19, 30, 40),
        titel = "bar"
    )
    val metaJson = "{\"id\":\"e58ed763-928c-4155-bee9-fdbaaadc15f3\",\"created\":\"2015-07-29 19:30:40\",\"titel\":\"bar\"}"

    val dokument = Dokument(
        meta,
        listOf("test.pdf")
    )
    val dokumentJson = "{\"meta\":{\"id\":\"e58ed763-928c-4155-bee9-fdbaaadc15f3\",\"created\":\"2015-07-29 19:30:40\",\"titel\":\"bar\"},\"inhalt\":[\"test.pdf\"]}"

    val emptyDokument = Dokument(
        meta,
        emptyList()
    )
    val emptyDokumentJson = "{\"meta\":{\"id\":\"e58ed763-928c-4155-bee9-fdbaaadc15f3\",\"created\":\"2015-07-29 19:30:40\",\"titel\":\"bar\"},\"inhalt\":[]}"


    @Test
    fun testDokumentMetaSerialization() {
        val json = Json.encodeToString(meta)
        assertEquals(metaJson, json)
    }

    @Test
    fun testDokumentMetaDeserialization() {
        val decodedMeta = Json.decodeFromString<DokumentMeta>(metaJson)
        assertEquals(meta, decodedMeta)
    }

    @Test
    fun testDokumentSerialization() {
        val json = Json.encodeToString(dokument)
        assertEquals(dokumentJson, json)
    }

    @Test
    fun testDokumentDeserialization() {
        val decodedDokument = Json.decodeFromString<Dokument>(dokumentJson)
        assertEquals(dokument, decodedDokument)
    }

    @Test
    fun testEmptyDokumentSerialization() {
        val json = Json.encodeToString(emptyDokument)
        assertEquals(emptyDokumentJson, json)
    }

    @Test
    fun testEmptyDokumentDeserialization() {
        val decodedDokument = Json.decodeFromString<Dokument>(emptyDokumentJson)
        assertEquals(emptyDokument, decodedDokument)
    }
}