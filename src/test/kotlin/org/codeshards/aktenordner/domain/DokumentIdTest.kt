package org.codeshards.aktenordner.domain

import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class DokumentIdTest {

    @Test
    fun testIdSerialization() {
        val input = DokumentId()
        val text = Json.encodeToString(input)
        assertTrue { text.contains(input.instance.toString()) }
    }

    @Test
    fun testIdListSerialization() {
        val input = DokumentId()
        val list = listOf<DokumentId>(input)
        val text = Json.encodeToString(list)
        assertTrue { text.contains(input.shortString()) }
    }

    @Test
    fun testIdDeserialization() {
        val id = "742fa5b6-5b85-449c-8efb-8461e210ca88"
        val text = "\"$id\""
        val dokumentId = Json.decodeFromString<DokumentId>(text)
        assertEquals(id, dokumentId.instance.toString())
    }

//    @Test
//    fun testToString() {
//        val id = "742fa5b6-5b85-449c-8efb-8461e210ca88"
//        val text = "\"$id\""
//        val dokumentId = Json.decodeFromString<DokumentId>(text)
//        // TODO: why is AggregateId.toString not used?
//        println(dokumentId.toString())
//    }
}