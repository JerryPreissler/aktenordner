package org.codeshards.aktenordner.application.service

import io.mockk.every
import io.mockk.mockkClass
import org.codeshards.aktenordner.adapter.outgoing.persistence.memoryPersistenceKoinTestModule
import org.codeshards.aktenordner.application.port.incoming.CreateDokumentCommand
import org.codeshards.aktenordner.application.port.outgoing.CreateDokumentPort
import org.codeshards.aktenordner.domain.Dokument
import org.codeshards.aktenordner.domain.DokumentMeta
import org.codeshards.aktenordner.domain.DokumentId
import org.codeshards.ddd.OperationResult
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.RegisterExtension
import org.koin.test.KoinTest
import org.koin.test.junit5.KoinTestExtension
import org.koin.test.junit5.mock.MockProviderExtension
import org.koin.test.mock.declareMock
import java.time.LocalDateTime

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CreateDokumentServiceTest : KoinTest {

    @JvmField
    @RegisterExtension
    @Suppress("unused")
    val koinTestExtension = KoinTestExtension.create {
        modules(
            memoryPersistenceKoinTestModule,
        )
    }

    @JvmField
    @RegisterExtension
    val mockProvider = MockProviderExtension.create { clazz ->
        mockkClass(clazz)
    }

    val beforeTestTime = LocalDateTime.now()

    /**
     * Mock test
     * Not much to see here really, just a first test showing how to use mocking.
     */
    @Test
    fun createDokumentTest() {
        val command = CreateDokumentCommand(titel = "foobar")
        val expectedId = DokumentId()
        val expected = Dokument(
            DokumentMeta(
                id = expectedId,
                titel = "foobar"
            ),
            emptyList()
        )
        declareMock<CreateDokumentPort> {
            every { createDokument(any()) } returns OperationResult.SUCCESS<Dokument>(expected)
        }
        val testee = CreateDokumentService()
        val after = LocalDateTime.now()
        when (val result = testee.createDokument(command)) {
            is OperationResult.SUCCESS -> {
                val value = result.value
                assertEquals(expected.meta.titel, value.meta.titel)
                assertEquals(expectedId , value.id)
                assertTrue(beforeTestTime.isBefore(value.meta.created))
                assertTrue(after.isAfter(value.meta.created))
            }
            else -> fail("Unexpected result ${result}")
        }
    }

}