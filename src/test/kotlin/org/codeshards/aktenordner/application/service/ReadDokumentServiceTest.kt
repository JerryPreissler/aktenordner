package org.codeshards.aktenordner.application.service

import io.mockk.every
import io.mockk.mockkClass
import org.codeshards.aktenordner.adapter.outgoing.persistence.memoryPersistenceKoinTestModule
import org.codeshards.aktenordner.application.port.outgoing.ReadDokumentPort
import org.codeshards.aktenordner.domain.Dokument
import org.codeshards.aktenordner.domain.DokumentMeta
import org.codeshards.aktenordner.domain.DokumentId
import org.codeshards.ddd.OperationResult
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.RegisterExtension
import org.koin.test.KoinTest
import org.koin.test.junit5.KoinTestExtension
import org.koin.test.junit5.mock.MockProviderExtension
import org.koin.test.mock.declareMock

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ReadDokumentServiceTest : KoinTest {

    @JvmField
    @RegisterExtension
    @Suppress("unused")
    val koinTestExtension = KoinTestExtension.create {
        modules(
            memoryPersistenceKoinTestModule,

        )
    }

    @JvmField
    @RegisterExtension
    val mockProvider = MockProviderExtension.create { clazz ->
        mockkClass(clazz)
    }

    /**
     * Mock test
     * Not much to see here really, just a first test showing how to use mocking.
     */
    @Test
    fun mockTest() {
        val testId = DokumentId()
        val expected = Dokument(
            DokumentMeta(id = testId),
            emptyList()
        )
        val testId2 = DokumentId()
        declareMock<ReadDokumentPort> {
            every { readDokument(testId) } returns OperationResult.SUCCESS<Dokument>(expected)
            every { readDokument(testId2) } returns OperationResult.ERROR(message = "nope")
        }
        val testee = ReadDokumentService()
        when (val result = testee.getDokument(testId)) {
            is OperationResult.SUCCESS -> assertEquals(expected, result.value)
            else -> fail("Unexpected result ${result}")
        }
        when (val result = testee.getDokument(testId2)) {
            is OperationResult.ERROR -> assertEquals("nope", result.message)
            else -> fail("Unexpected result ${result}")
        }
    }

}