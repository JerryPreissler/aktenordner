package org.codeshards.aktenordner.application.port.incoming

import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions.*

class CreateDokumentCommandTests {

    @Test
    fun testCreateDokumentCommandSerialization() {
        val command = CreateDokumentCommand(titel = "foo")
        val json = Json.encodeToString(command)
        assertEquals("{\"titel\":\"foo\"}", json)
    }

    @Test
    fun testCreateDokumentCommandDeserialization() {
        val json = "{\"titel\":\"foo\"}"
        val command = Json.decodeFromString<CreateDokumentCommand>(json)
        assertEquals(CreateDokumentCommand(titel = "foo"), command)
    }
}