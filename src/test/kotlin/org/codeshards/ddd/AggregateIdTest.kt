package org.codeshards.ddd

import java.util.UUID
import kotlinx.serialization.Serializable
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import org.codeshards.aktenordner.domain.DokumentId
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions.*
import kotlin.test.assertFailsWith

private const val validUuidString = "e58ed763-928c-4155-bee9-fdbaaadc15f3"
private const val invalidUuidString = "123"
private const val validUuidsJson: String = "{\"uuids\": [\"e58ed763-928c-4155-bee9-fdbaaadc15f3\",\"123e4567-e89b-12d3-a456-426614174000\",\"00000000-0000-0000-0000-000000000000\"]}"


/**
 * Test AggregateId using DokumentId as first concrete implementation
 */
class AggregateIdTest {

    @Serializable
    data class UUIDList(val uuids: List<DokumentId>)

    @Test
    fun testInstantiationOk() {
        val result = DokumentId.Builder().create(validUuidString)
        when (result) {
            is OperationResult.SUCCESS<DokumentId> -> {
                assertEquals(result.value.instance, UUID.fromString(validUuidString))
            }
            else -> fail("Unexpected result for AggregateId from $validUuidString, got $result")
        }
    }

    @Test
    fun testDeserializationOk() {
        val test = Json.decodeFromString<UUIDList>(validUuidsJson)
        test.uuids.forEach { it: DokumentId ->
            assertEquals(it.instance, UUID.fromString(it.instance.toString()))
        }
    }

    @Test
    fun testInstantiationFail() {
        val result = DokumentId.Builder().create(invalidUuidString)
        when (result) {
            is OperationResult.ERROR -> {
                assertEquals("Invalid UUID string: $invalidUuidString", (result.cause as IllegalArgumentException).message)
            }
            else -> fail("Unexpected result for AggregateId from $validUuidString, got $result")
        }
    }

    @Test
    fun testDeserializationFail() {
        val invalidUuidValue = "{\"uuids\": [\"$invalidUuidString\"]}"
        assertFailsWith(
            exceptionClass = IllegalArgumentException::class,
            message = "Invalid UUID $invalidUuidString",
            block = {
                Json.decodeFromString<UUIDList>(invalidUuidValue)
            }
        )
    }

    @Test
    fun testWorksWithMap() {
        val id_as_string = "a89c2a6b-912c-46dd-a080-e412d7de27c1"
        val map: MutableMap<AggregateId, String> = mutableMapOf()
        val id_put = (DokumentId.Builder().create(id_as_string) as OperationResult.SUCCESS).value
        assertNull(map.get(id_put))
        map.put(id_put, "foobar")
        val id_get = (DokumentId.Builder().create(id_as_string) as OperationResult.SUCCESS).value
        val result = map.get(id_get)
        assertNotNull(result)
        assertEquals(result, "foobar")
    }
}