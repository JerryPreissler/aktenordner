package org.codeshards.aktenordner.domain

import kotlinx.serialization.Serializable
import org.codeshards.ddd.AggregateId
import java.util.UUID

@Serializable(with = DokumentIdSerializer::class)
data class DokumentId(override val instance: UUID = UUID.randomUUID(),):
    AggregateId(
        instance = instance,
        context = "aktenordner",
        type = "dokument"
) {
    class Builder: AggregateId.Builder<DokumentId>() {
        override fun create(uuid: UUID): DokumentId {
            return DokumentId(instance = uuid)
        }
    }

}

