package org.codeshards.aktenordner.domain

import kotlinx.serialization.Serializable
import java.time.LocalDateTime

@Serializable
data class Dokument(
    val meta: DokumentMeta,
    val inhalt: List<String>
) {
    val id: DokumentId
        get() = meta.id
}

@Serializable
data class DokumentMeta(
    val id: DokumentId,
    @Serializable(with = LocalDateTimeSerializer::class)
    val created: LocalDateTime = LocalDateTime.now(),
    val titel: String? = null
)
