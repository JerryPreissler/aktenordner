package org.codeshards.aktenordner.domain

import kotlinx.serialization.KSerializer
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import org.codeshards.ddd.AggregateId
import org.codeshards.ddd.OperationResult
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

open class AggregateIdSerializer<A: AggregateId> {
    fun deserialize(id: String, builder: AggregateId.Builder<A>): A {
        val idResult = builder.create(id)
        when (idResult) {
            is OperationResult.ERROR -> throw IllegalArgumentException("Invalid UUID " + id)
            is OperationResult.SUCCESS-> return idResult.value
        }
    }
    fun serializeAggregateId(encoder: Encoder, value: AggregateId) {
        encoder.encodeString(value.instance.toString())
    }
}

object DokumentIdSerializer: AggregateIdSerializer<DokumentId>(), KSerializer<DokumentId> {
    override val descriptor= PrimitiveSerialDescriptor("DokumentId", PrimitiveKind.STRING)
    override fun deserialize(decoder: Decoder): DokumentId {
        val id: String = decoder.decodeString()
        return deserialize(id, DokumentId.Builder())
    }

    override fun serialize(encoder: Encoder, value: DokumentId) {
        encoder.encodeString(value.shortString())
    }

}

object LocalDateTimeSerializer : KSerializer<LocalDateTime> {
    override val descriptor = PrimitiveSerialDescriptor("DateTime", PrimitiveKind.STRING)
    private val dateformat = "yyyy-MM-dd HH:mm:ss"
    private val formatter = DateTimeFormatter.ofPattern(dateformat)

    override fun deserialize(decoder: Decoder): LocalDateTime {
        val decodedString = decoder.decodeString()
        val localDateTime = LocalDateTime.parse(decodedString, formatter)
        return localDateTime
    }

    override fun serialize(encoder: Encoder, value: LocalDateTime) {
        encoder.encodeString(formatter.format(value))
    }
}