package org.codeshards.aktenordner.application.port.incoming

import kotlinx.serialization.Serializable
import org.codeshards.aktenordner.domain.DokumentId
import java.io.InputStream

@Serializable
data class CreateDokumentCommand(val titel: String? = null) {
}

data class UploadDokumentInhaltCommand(
 val id: DokumentId,
 val filename: String?,
 val inputStream: InputStream
) {
}