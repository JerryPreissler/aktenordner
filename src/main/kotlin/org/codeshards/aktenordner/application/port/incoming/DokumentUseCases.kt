package org.codeshards.aktenordner.application.port.incoming

import org.codeshards.aktenordner.domain.Dokument
import org.codeshards.aktenordner.domain.DokumentId
import org.codeshards.ddd.OperationResult

interface GetDokumentUseCase {
    fun getDokument(id: DokumentId): OperationResult<Dokument>
}

interface CreateDokumentUseCase {
    fun createDokument(command: CreateDokumentCommand): OperationResult<Dokument>
}

interface UploadDokumentContentUseCase {
    fun uploadContent(command: UploadDokumentInhaltCommand): OperationResult<Dokument>
}

interface ListDokumenteUseCase {
    fun listDokumente(): OperationResult<List<DokumentId>>
}