package org.codeshards.aktenordner.application.port.outgoing

import org.codeshards.aktenordner.application.port.incoming.UploadDokumentInhaltCommand
import org.codeshards.aktenordner.domain.Dokument
import org.codeshards.aktenordner.domain.DokumentMeta
import org.codeshards.aktenordner.domain.DokumentId
import org.codeshards.ddd.OperationResult

interface ListDokumentePort {
    fun listDokumente(): OperationResult<List<DokumentId>>
}

interface ReadDokumentPort {

    fun readDokument(id: DokumentId): OperationResult<Dokument>

}

interface CreateDokumentPort {
    fun createDokument(meta: DokumentMeta): OperationResult<Dokument>
}

interface UploadDokumentContentPort {
    fun uploadContent(command: UploadDokumentInhaltCommand): OperationResult<Dokument>
}