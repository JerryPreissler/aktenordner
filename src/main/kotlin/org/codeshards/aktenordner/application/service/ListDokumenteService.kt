package org.codeshards.aktenordner.application.service

import org.codeshards.aktenordner.application.port.incoming.ListDokumenteUseCase
import org.codeshards.aktenordner.application.port.outgoing.ListDokumentePort
import org.codeshards.aktenordner.domain.DokumentId
import org.codeshards.ddd.OperationResult
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class ListDokumenteService: ListDokumenteUseCase, KoinComponent {

    private val listDokumentePort by inject<ListDokumentePort>()
    override fun listDokumente(): OperationResult<List<DokumentId>> {
        return listDokumentePort.listDokumente()
    }

}