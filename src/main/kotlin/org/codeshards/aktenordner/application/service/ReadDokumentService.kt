package org.codeshards.aktenordner.application.service

import org.codeshards.aktenordner.application.port.incoming.GetDokumentUseCase
import org.codeshards.aktenordner.application.port.outgoing.ReadDokumentPort
import org.codeshards.aktenordner.domain.Dokument
import org.codeshards.aktenordner.domain.DokumentId
import org.codeshards.ddd.OperationResult
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class ReadDokumentService: GetDokumentUseCase, KoinComponent {

    private val readDokumentPort by inject<ReadDokumentPort>()
    override fun getDokument(id: DokumentId): OperationResult<Dokument> {
        return readDokumentPort.readDokument(id)
    }

    class DokumentNotFoundException(msg: String?): Throwable(msg) {
        companion object {}
    }
}