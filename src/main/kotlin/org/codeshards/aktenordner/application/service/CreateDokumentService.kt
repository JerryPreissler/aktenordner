package org.codeshards.aktenordner.application.service

import org.codeshards.aktenordner.application.port.incoming.CreateDokumentCommand
import org.codeshards.aktenordner.application.port.incoming.CreateDokumentUseCase
import org.codeshards.aktenordner.application.port.outgoing.CreateDokumentPort
import org.codeshards.aktenordner.domain.Dokument
import org.codeshards.aktenordner.domain.DokumentMeta
import org.codeshards.aktenordner.domain.DokumentId
import org.codeshards.ddd.OperationResult
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class CreateDokumentService: CreateDokumentUseCase, KoinComponent {

    private val createDokumentPort by inject<CreateDokumentPort>()
    override fun createDokument(command: CreateDokumentCommand): OperationResult<Dokument> {
        val id = DokumentId()
        val dokumentMeta = DokumentMeta(
            id = id,
            titel = command.titel
        )
        return createDokumentPort.createDokument(dokumentMeta)
    }

}