package org.codeshards.aktenordner.application.service

import org.codeshards.aktenordner.application.port.incoming.*
import org.codeshards.aktenordner.application.port.outgoing.UploadDokumentContentPort
import org.codeshards.aktenordner.domain.Dokument
import org.codeshards.ddd.OperationResult
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class UploadDokumentContentService: UploadDokumentContentUseCase, KoinComponent {

    private val uploadDokumentContentPort by inject<UploadDokumentContentPort>()
    override fun uploadContent(command: UploadDokumentInhaltCommand): OperationResult<Dokument> {
        return uploadDokumentContentPort.uploadContent(command)
    }

}