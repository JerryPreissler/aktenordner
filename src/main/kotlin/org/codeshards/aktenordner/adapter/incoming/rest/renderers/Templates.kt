package org.codeshards.aktenordner.adapter.incoming.rest.renderers

import kotlinx.html.BODY
import kotlinx.html.FlowContent

/**
 * Template to generate an HTML page containing only the content provided by the
 * function passed as a parameter
 *
 * @param content
 * @receiver
 */
fun BODY.contentOnly(content: FlowContent.() -> Unit) {
    content()
}