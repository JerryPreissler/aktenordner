package org.codeshards.aktenordner.adapter.incoming.rest

import io.ktor.http.*
import io.ktor.http.content.*
import io.ktor.serialization.kotlinx.json.*
import io.ktor.server.plugins.contentnegotiation.*
import io.ktor.server.application.*
import io.ktor.server.html.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import kotlinx.html.*
import org.codeshards.aktenordner.adapter.incoming.rest.renderers.contentOnly
import org.codeshards.aktenordner.adapter.incoming.rest.renderers.idListHtml
import org.codeshards.aktenordner.application.port.incoming.*
import org.koin.ktor.ext.inject
import org.codeshards.aktenordner.application.service.ReadDokumentService
import org.codeshards.aktenordner.domain.Dokument
import org.codeshards.aktenordner.domain.DokumentId
import org.codeshards.ddd.OperationResult
import java.time.LocalDateTime

fun Application.dokumentModule() {

    install(ContentNegotiation) {
        json()
    }

    val getDokumentHandler by inject<GetDokumentUseCase>()
    val createDokumentHandler by inject<CreateDokumentUseCase>()
    val listDokumenteHandler by inject<ListDokumenteUseCase>()
    val uploadDokumentContentHandler by inject<UploadDokumentContentUseCase>()

    routing {
        route("/aktenordner"){
            get("/dokument"){
                handleListDokumente(call, listDokumenteHandler)
            }
            post("/dokument") {
                handlePostDokument(call, createDokumentHandler)
            }
            get("/dokument/{id}"){
                handleGetDokumentById(call, getDokumentHandler)
            }
            post("/dokument/{id}") {
                handlePostDokumentInhalt(call, uploadDokumentContentHandler)
            }
        }
    }
}

private suspend fun handlePostDokumentInhalt(
    call: ApplicationCall,
    uploadDokumentContentHandler: UploadDokumentContentUseCase
) {
    val idResult =
        extractId(call)
    when (idResult) {
        is OperationResult.SUCCESS<DokumentId> -> {
            handlePostDokumentInhaltWithId(call, idResult.value, uploadDokumentContentHandler)
        }
        is OperationResult.ERROR -> {
            call.response.status(HttpStatusCode.BadRequest)
            call.respondText { "Could not extract id from request. Cause:\n${idResult.message}" }
        }
        null -> {
            call.response.status(HttpStatusCode.BadRequest)
            call.respondText { "No id supplied" }
        }
    }
}

private suspend fun handlePostDokumentInhaltWithId(
    call: ApplicationCall,
    id: DokumentId,
    uploadDokumentContentHandler: UploadDokumentContentUseCase
) {
    val multipart = call.receiveMultipart()
    multipart.forEachPart { part ->
        when (part) {
            is PartData.BinaryChannelItem -> {
                call.response.status(HttpStatusCode.BadRequest)
                call.respondText { "Unexpected form part BinaryChannelItem: $part" }
            }
            is PartData.BinaryItem -> {
                call.response.status(HttpStatusCode.BadRequest)
                call.respondText { "Unexpected form part BinaryItem: $part" }
            }
            is PartData.FileItem -> {
                val name = part.originalFileName
                val inputStream = part.streamProvider()
                val command = UploadDokumentInhaltCommand(
                    id, name, inputStream
                )
                val result = uploadDokumentContentHandler.uploadContent(command)
                when (result) {
                    is OperationResult.ERROR -> {
                        with(call) {
                            handleGenericError(result)
                        }
                    }
                    is OperationResult.SUCCESS -> {
                        val path = "/aktenordner/dokument/${result.value.id.instance}"
                        with(call) {
                            response.headers.append(HttpHeaders.Location, path)
                            response.status(HttpStatusCode.Created)
                            respond(result.value)
                        }
                    }
                }
            }

            is PartData.FormItem -> {
                println("received $part")
            }
        }
        part.dispose
    }
}

val CONTENT_TYPE_JSON = "application/json"
val CONTENT_TYPE_FORM_ENCODED = "application/x-www-form-urlencoded"
private suspend fun handlePostDokument(call: ApplicationCall, createDokumentHandler: CreateDokumentUseCase) {
    val contentType = call.request.headers.get(HttpHeaders.ContentType)
        ?.split(";")?.get(0)
    val command: CreateDokumentCommand? =  when(contentType) {
            // TODO: provide error message on JsonDecodingException
            CONTENT_TYPE_JSON -> call.receive<CreateDokumentCommand>()
            CONTENT_TYPE_FORM_ENCODED -> fromParameters(call.receiveParameters())
            else -> null
        }
    val result = command?.let {
        createDokumentHandler.createDokument(command)
    } ?: run {
        OperationResult.ERROR("unknown content type $contentType", status = HttpStatusCode.UnsupportedMediaType)
    }
    with(call) {
        when (result) {
            is OperationResult.SUCCESS -> {
                val path = "/aktenordner/dokument/${result.value.id.instance}"
                response.headers.append(HttpHeaders.Location, path)
                response.status(HttpStatusCode.Created)
                respond(result.value)
            }
            is OperationResult.ERROR -> {
                handleGenericError(result)
            }
        }
    }
}

private suspend fun handleListDokumente(
    call: ApplicationCall,
    listDokumenteHandler: ListDokumenteUseCase
) {
    val result = listDokumenteHandler.listDokumente()
    when (result) {
        is OperationResult.SUCCESS -> {
            val idList = result.value
//            val detailsFun = FlowContent.idListHtml(idList)
            call.respondHtml(HttpStatusCode.OK) {
                head {  }
                body {
                   contentOnly({idListHtml(idList)})
                }
            }
        }
        is OperationResult.ERROR -> {
            call.handleGenericError(result)
        }
    }
}

private suspend fun handleGetDokumentById(
    call: ApplicationCall,
    getDokumentHandler: GetDokumentUseCase
) {
    val idResult =
        extractId(call)
    when (idResult) {
        is OperationResult.SUCCESS<DokumentId> -> {
            val result = getDokumentHandler.getDokument(idResult.value)
            with(call) {
                when (result) {
                    is OperationResult.SUCCESS<Dokument> ->
                        respond(result.value)
                    is OperationResult.ERROR -> {
                        when (result.cause) {
                            is ReadDokumentService.DokumentNotFoundException ->
                                respond(HttpStatusCode.NotFound, result.message)
                            else -> handleGenericError(result)
                        }
                    }
                }
            }
        }
        is OperationResult.ERROR -> {
            call.respond(HttpStatusCode.BadRequest, idResult.message)
        }
        null -> {
            call.respond(HttpStatusCode.BadRequest, "No id specified")
        }
    }
}

private fun extractId(call: ApplicationCall): OperationResult<DokumentId>? {
    val id = call.parameters["id"]
    val idResult =
        id?.let { it1 ->
            DokumentId.Builder().create(it1)
        }
    return idResult
}

private suspend fun ApplicationCall.handleGenericError(result: OperationResult.ERROR) {
    val now = LocalDateTime.now()
    val msg = buildString {
        append("At $now unexpected error ${result.message}")
            .append("(cause: ${result.cause?.message})")
    }
    respond(result.status, msg)
}

fun fromParameters(parameters: Parameters): CreateDokumentCommand {
    return CreateDokumentCommand(
        titel = parameters.get("titel")
    )
}
