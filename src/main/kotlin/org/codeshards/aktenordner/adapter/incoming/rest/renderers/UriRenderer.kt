package org.codeshards.aktenordner.adapter.incoming.rest.renderers

import io.ktor.http.*
import org.codeshards.ddd.AggregateId

class UriRenderer {
    companion object {

        val BASE_URL = URLBuilder(
            protocol = URLProtocol.HTTP,
            host = "localhost",
            port = 8080,
        ).build()

        fun render(id: AggregateId): Url {
            return URLBuilder(BASE_URL)
                .appendPathSegments(
                    id.context,
                    id.type,
                    id.instance.toString()
                )
                .build()
        }
    }
}