package org.codeshards.aktenordner.adapter.incoming.rest

import org.codeshards.aktenordner.application.port.incoming.GetDokumentUseCase
import org.codeshards.aktenordner.application.service.ReadDokumentService
import org.codeshards.aktenordner.application.service.CreateDokumentService
import org.codeshards.aktenordner.application.service.ListDokumenteService
import org.codeshards.aktenordner.application.service.UploadDokumentContentService
import org.codeshards.aktenordner.application.port.incoming.CreateDokumentUseCase
import org.codeshards.aktenordner.application.port.incoming.ListDokumenteUseCase
import org.codeshards.aktenordner.application.port.incoming.UploadDokumentContentUseCase
import org.koin.dsl.module
import org.koin.core.module.dsl.singleOf
import org.koin.core.module.dsl.bind

val restKoinModule = module {
    singleOf(::CreateDokumentService) {
        bind<CreateDokumentUseCase>()
    }
    singleOf(::ReadDokumentService) {
        bind<GetDokumentUseCase>()
    }
    singleOf(::ListDokumenteService) {
        bind<ListDokumenteUseCase>()
    }
    singleOf(::UploadDokumentContentService) {
        bind<UploadDokumentContentUseCase>()
    }
}