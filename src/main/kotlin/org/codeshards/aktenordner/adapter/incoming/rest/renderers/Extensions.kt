package org.codeshards.aktenordner.adapter.incoming.rest.renderers

import kotlinx.html.*
import org.codeshards.aktenordner.domain.DokumentId

/**
 * Extension functions to render domain objects to HTML/JSON
 *
 * Hint: place in alphabetic order of domain object
 */
@HtmlTagMarker
fun FlowContent.idListHtml(idList: List<DokumentId>) {
    ul {
        idList.forEach { it ->
            li {
                a(UriRenderer.render(it).toString()) { +it.shortString() }
            }
        }
    }
}