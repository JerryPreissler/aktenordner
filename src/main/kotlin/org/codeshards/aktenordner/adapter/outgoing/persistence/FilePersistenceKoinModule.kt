package org.codeshards.aktenordner.adapter.outgoing.persistence

import org.codeshards.aktenordner.application.port.outgoing.ListDokumentePort
import org.codeshards.aktenordner.application.port.outgoing.ReadDokumentPort
import org.codeshards.aktenordner.application.port.outgoing.CreateDokumentPort
import org.codeshards.aktenordner.application.port.outgoing.UploadDokumentContentPort
import org.koin.core.module.dsl.bind
import org.koin.core.module.dsl.withOptions
import org.koin.dsl.module

fun getBaseDirectory(): String = "./data"

val filePersistenceKoinModule = module {
    single {
        DokumentFilePersistenceAdapter(
            getBaseDirectory()
        )
    } withOptions {
        bind<ReadDokumentPort>()
        bind<CreateDokumentPort>()
        bind<ListDokumentePort>()
        bind<UploadDokumentContentPort>()
    }

}