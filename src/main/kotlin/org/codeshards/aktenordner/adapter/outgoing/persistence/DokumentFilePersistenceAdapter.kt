package org.codeshards.aktenordner.adapter.outgoing.persistence

import io.ktor.http.*
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import mu.KLogging
import org.codeshards.aktenordner.application.port.incoming.UploadDokumentInhaltCommand
import org.codeshards.aktenordner.application.port.outgoing.ListDokumentePort
import org.codeshards.aktenordner.application.port.outgoing.ReadDokumentPort
import org.codeshards.aktenordner.application.port.outgoing.CreateDokumentPort
import org.codeshards.aktenordner.application.port.outgoing.UploadDokumentContentPort
import org.codeshards.aktenordner.application.service.ReadDokumentService
import org.codeshards.aktenordner.domain.Dokument
import org.codeshards.aktenordner.domain.DokumentMeta
import org.codeshards.aktenordner.domain.DokumentId
import org.codeshards.ddd.OperationResult
import java.nio.charset.StandardCharsets
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import kotlin.io.path.bufferedWriter
import kotlin.io.path.listDirectoryEntries
import kotlin.streams.toList

private const val METADATA_FILENAME = "metadata.json"

class DokumentFilePersistenceAdapter(baseDirectory: String)
    : ReadDokumentPort, CreateDokumentPort, ListDokumentePort, UploadDokumentContentPort {

    companion object: KLogging()

    // just need an instance of DokumentId to get context and type
    private val DUMMY_ID = DokumentId()
    // root directory for all repositories
    val reposRoot = Paths.get(baseDirectory)
    // root directory for Dokument repository
    val dataRoot = Paths.get(baseDirectory, DUMMY_ID.context, DUMMY_ID.type)

    constructor(baseDirectory: String, entries: List<DokumentMeta>): this(baseDirectory) {
        logger.error("Setting up at $baseDirectory")
        for(entry in entries) {
            createDokument(entry)
        }
    }
    init {
        logger.debug("Setting up at $baseDirectory")
        // check that base directory is present
        val isDir = Files.isDirectory(reposRoot)
        if (!isDir) {
            throw NoSuchFileException(
                reposRoot.toFile(),
                reason = "configured data directory not found"
            )
        }
        // create dataRoot directory if not present
        if (!Files.isDirectory(dataRoot)) {
            Files.createDirectories(dataRoot)
        }
    }
    override fun listDokumente(): OperationResult<List<DokumentId>> {
        val dokumentIds = Files.walk(dataRoot, 1)
            .map { it -> DokumentId.Builder().create(it.fileName.toString()) }
            .filter { it is OperationResult.SUCCESS<DokumentId> } // todo: add logging for fails?
            .map { (it as OperationResult.SUCCESS<DokumentId>).value }
            .toList()
        return OperationResult.SUCCESS(dokumentIds)
    }

    override fun readDokument(id: DokumentId): OperationResult<Dokument> {
        val metaPath = metaPath(id)
        val path = metaPath.parent
        if (!Files.isRegularFile(metaPath)) {
            return OperationResult.ERROR(
                "No Dokument found for ${id.toString()}",
                ReadDokumentService.DokumentNotFoundException(id.toString()))
        }
        val text = Files.readString(metaPath, StandardCharsets.UTF_8)
        val dokumentMeta = Json.decodeFromString<DokumentMeta>(text)
        val inhalte = path.listDirectoryEntries("*.{pdf}")
                          .map { it -> it.fileName.toString() }
        val dokument = Dokument(dokumentMeta, inhalte)
        return OperationResult.SUCCESS(dokument)
    }

    /**
     * Create directory and metadata file for a new dokument
     * Note that the directory indicated by the dokument's id must not already exist
     *
     * @param meta
     * @return OperationResult with the outcome of the function
     */
    override fun createDokument(meta: DokumentMeta): OperationResult<Dokument> {
        val dokumentPath = path(meta.id)
        if (Files.exists(dokumentPath)) {
            return OperationResult.ERROR(
                "Dokument ${meta.id} already exisits",
                status = HttpStatusCode.BadRequest
            )
        }
        Files.createDirectory(dokumentPath)
        val dokumentString = Json.encodeToString(meta)
        val metadata = Files.createFile(metaPath(meta.id))
        metadata.bufferedWriter().use {
            out -> out.write(dokumentString)
        }
        return OperationResult.SUCCESS<Dokument>(
            Dokument(meta, emptyList())
        )
    }

    override fun uploadContent(command: UploadDokumentInhaltCommand): OperationResult<Dokument> {
        // sanitize filename
        // ensure file does not exist
        val path = path(command.id, command.filename)
        return if (!Files.exists(path)) {
            if (Files.isDirectory(path.parent)) {
                command.inputStream.use { input ->
                    Files.copy(input, path)
                }
                readDokument(command.id)
            } else {
                // parent dir for dokument does not exist
                OperationResult.ERROR(
                    "Dokument ${command.id} does not exist",
                    status = HttpStatusCode.NotFound
                )
            }
        } else {
            // file already exists for dokument
            OperationResult.ERROR(
                "Dokument ${command.filename} already exist for dokument",
                status = HttpStatusCode.BadRequest
            )
        }
    }

    /**
     * Path
     * Note that this function does not check if the directory actually exists
     *
     * @param id of a Dokument
     * @return path to the directory containing the dokument's data
     */
    private fun path(id: DokumentId, filename: String? = null): Path =
        Paths.get(dataRoot.toString(), id.shortString(), filename ?: "")

    /**
     * Meta path
     * Note that this function does not check if the metadata file actually exists
     *
     * @param id of a Dokument
     * @return path to the file containing the dokument's metadata file
     */
    private fun metaPath(id: DokumentId): Path =
        Paths.get(dataRoot.toString(), id.shortString(), METADATA_FILENAME)
}