package org.codeshards.ddd

interface Aggregate {
    fun getBoundedContext(): String
    fun getType(): String
}