package org.codeshards.ddd

import io.ktor.http.*

/**
 * Generic result class for operations returning a value or an error
 */
sealed class OperationResult<out T> {
        data class SUCCESS<T>(val value: T): OperationResult<T>()
        data class ERROR(
                val message: String,
                val cause: Throwable? = null,
                val status: HttpStatusCode = HttpStatusCode.InternalServerError
        ): OperationResult<Nothing>()
}