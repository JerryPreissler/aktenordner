package org.codeshards.ddd

import java.util.UUID

abstract class AggregateId (
    open val instance: UUID = UUID.randomUUID(),
    val context: String,
    val type: String,
)
{

    override fun toString(): String {
        return "${context}/${type}/${instance.toString()}"
    }

    fun shortString(): String {
        return instance.toString()
    }

    abstract class Builder<A>() {

        abstract fun create(uuid: UUID): A

        fun create(id: String): OperationResult<A> {
            return try {
                val uuid = UUID.fromString(id)
                OperationResult.SUCCESS<A>(this.create(uuid))
            } catch (e: Exception) {
                OperationResult.ERROR("Invalid UUID format: " + id, e)
            }
        }
    }

}

