package org.codeshards

import io.ktor.server.application.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import org.codeshards.aktenordner.adapter.incoming.rest.dokumentModule
import org.codeshards.aktenordner.adapter.incoming.rest.restKoinModule
import org.codeshards.aktenordner.adapter.outgoing.persistence.filePersistenceKoinModule
import org.codeshards.plugins.*
import org.koin.core.context.startKoin
import org.koin.ktor.plugin.Koin
import org.koin.logger.slf4jLogger

fun main() {
    embeddedServer(Netty, port = 8080, host = "0.0.0.0", module = Application::module)
        .start(wait = true)
}

//fun Application.module() {
//    startKoin {
//        slf4jLogger()
//        modules(
//            restKoinModule,
//            filePersistenceKoinModule
//        )
//    }
//    dokumentModule()
//    configureRouting()
//}

fun Application.module() {
    dokumentModule()
    configureRouting()
    install(Koin) {
        slf4jLogger()
        modules(
            restKoinModule,
            filePersistenceKoinModule
        )
    }
}